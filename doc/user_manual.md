# Smartitle User Manual

Thanks for using Smartitle! It has all the features you will ever want (probably) in term of letting normal members of a group to add and remove parts of the group title. This manual explains all of them.

## The Basics

If you have never used a Telegram bot before, the concept of "command" may sound scary. But in fact, it's just a message starting with a slash (`/`) and a command name. This message will be interpreted by a bot and it will do something for you. For example, the `/help` command asks the bot to display help information.

![The help command](images/help-command.png)

But keep in mind that if there are multiple bots in a group, a simple `/help` message may trigger other bots to display their help messages. To prevent that, append `@` and the username of the bot, in the case of the official Smartitle instance, it's `smartitle_bot`, if it's a self-hosted instance, check the bot's user profile page. Then, you should use `/help@smartitle_bot` in place of `/help`. This rule also applies to all other commands mentioned in this manual.

Now, to add a segment into the group title, you can use the `/add` command. This command accepts an argument which is a piece of text entered after the command part. So for example, if the original group title is `test` and you send `/add 🍎`, the group title will be changed to `🍎 | test`.

![The add command](images/add-command.png)

You can also use `/prepend` and `/unshift` command to add segments, they do the same thing as `/add`, just different names. You can also add segments to the end of the existing title instead of the beginning with `/push` or `/append`.

Once the title gets stuffed, it can be difficult to read the full title. The `/read` will come in handy in this situation. It lists all the segments for you beautifully.

If you don't like the content in a segment, you can use the `/remove` or `/shift` command. They also do the same thing, which is removing the first segment from the title. Suppose you really don't like apples, you can now remove it:

![Removing the apple](images/remove-command.png)

There is also a `/pop` command to remove things from the end, and a `/split` command which can be used to remove anything, but that is a little complicated for reasons we'll explain later.

## Complex Manipulation

`/split` and `/explode` command can split segments into more segments at the point of the text specified in the argument appears. For example, `/split t` will make `potato` to become `po | a | o`.

But it's actually more powerful (or complicated) than that. The argument for `/split` and `/explode` command is a regular expression rather than plain text. You can do things like "split the title at letter 'a' or 'o'" in one command: `/split a|o`, and that will make `potato` to become `p | t | t`. You can learn more about regular expressions by searching for tutorials online, do note that Smartitle supports only the syntax supported by [re2](https://github.com/google/re2).

When splitting a title, segments that ends up being empty are discarded, that's why you don't see `p | t | t | |` in the previous example. Using this feature, you can use `/split` or `/explode` command to remove segments anywhere in the title by just supplying them in the argument.

Reverse to splitting, the `/join` (or `/implode`, `/fusion`, `/concat`) joins all segments in the title together, replacing segment boundaries by its optional argument. So `/join e` will make `p | t | t` a `petet`, or just send `/join` if you still prefer using Telnet to browse the internet (now that's a joke no one understands).

The most advanced of the commands, `/replace`, does exactly what is says. To replace some text in the title to another, well, it's more straightforward with a screenshot, not that I'm too lazy to write or anything:

![Replacing things](images/replace-command.png)

Like `/split` and `/explode`, `/replace`'s argument is also a regular expression. Regular expressions always make things easier, right? ... Right?

`/clear` clears everything in the title except the first segment or the prefix which we'll talk about later.

`/undo` undoes the last change that was made through Smartitle, it can only undo one step, though, so don't make too many errors. `/undo` consumes no chances, which is also a concept we'll talk about later.

## Limiting Changes

Prefix is a piece of text, like other segments, but always appears at the beginning of the title. It's a good way to preserve group identity so the group name don't frequently change to something unrecognizable, and the members were like "When did I join this group?" and then probably quit. Prefix is set with `/set_prefix` command and the desired prefix supplied as the argument. Prefix is also cleared with `/set_prefix` command with no arguments. Only admins that can change group title themselves can use this command, of course.

Now we'd like to introduce the revolutionary "Chances" system. With this system enabled, it limits how many times a member can make changes to the title. Each member is given a default chance set by the admins, and using any commands that changes the title (apart from `/set_prefix` and `/undo`) consumes 1 chance. If their chances depleted, their commands will be denied. So users can not make very frequent changes and flooding the chat, which keeps everyone happy.

To gain chances, just chat in the group. Each message you send will give you the amount of chances specified by the chance conversion rate, which is also set by the admins. There is also a maximum amount of chance a member can keep, this value is set by the admins as well.

The `/set_default_chance`, `/set_chance_conversion_rate` and `/set_max_chance` commands can be used by admins that can change title themselves to set the numbers explained above.

These admins, of course, will also need to know the `/enable_change_limit` and `/disable_change_limit` commands to turn on or off the mechanism.

As a normal member, all you can do is to send `/limit_status` to see if the limit has been enabled, the 3 parameters, and the chances you have left.

## There's More?

Yes. There's more, but not long before the manual concludes.

`/history`, as its name sounds like, displays the history, the past changes to the title since the very beginning when this group has started using Smartitle. By default, it only shows the most recent 10 entries, but you can put a number in the arguments as how many entries away from the most recent one you would like to get. Like `/history 11` gives you the last 11-20 entries.

On the official Smartitle instance, you will be given a URL to a beautifully presented table in a web page. The link only works for 5 minutes, after which you need to request it again. This is unfortunately not present for self-hosted instances.

`/pin` has nothing to do with group titles, but it's still in Smartitle as a bonus feature. It allows a normal member to pin messages by replying the message they want to pin and type `/pin` and press send. An admin can disable it however, by not giving Smartitle the permission to "Pin messages".

Finally, this should be everything you need to know about using Smartitle. If you like this project, consider giving it a star on its [GitLab repository](https://gitlab.com/FiveYellowMice/smartitle). If there is a bug you want to report or a suggestion to say, submit an [issue](https://gitlab.com/FiveYellowMice/smartitle/issues). If you are a Ruby developer, you can also read the full code and perhaps improve on it. Thank you again for finishing this long manual, hope you enjoy your time with Smartitle.

------

CC-BY-SA 3.0

First written on 2019-05-17.
