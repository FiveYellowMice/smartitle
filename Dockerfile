FROM debian:bullseye

WORKDIR /opt/smartitle
COPY bin bin
COPY lib lib
COPY Gemfile Gemfile.lock .
COPY config.docker.rb config.rb

VOLUME /opt/smartitle/var

RUN ["/bin/bash", "-c", "apt-get update && apt-get install -y ruby ruby-dev bundler sqlite3 libsqlite3-dev libre2-9 libre2-dev && apt-get clean"]
RUN ["/bin/bash", "-c", "bundle config set --local path 'vendor/bundle' && bundle config set no-cache 'true' && bundle install"]
RUN ["/bin/bash", "-c", "apt-get remove -y ruby-dev libsqlite3-dev libre2-dev && apt-get autoremove -y"]

CMD ["/opt/smartitle/bin/smartitle"]
