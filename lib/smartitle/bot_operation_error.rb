# frozen_string_literal: true

module Smartitle

  class BotOperationError < StandardError

    class TitleTooLong < BotOperationError
    end

    class NotEnoughChances < BotOperationError
    end

  end

end
