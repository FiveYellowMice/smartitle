# frozen_string_literal: true

require 'active_support/core_ext/object/blank'
require 're2'
require 're2/string'
String.include(RE2::String)

module Smartitle

  module Utils

    if ENV['SMARTITLE_BACKTRACE']
      def self.print_error(e)
        (["#{e.class}: #{e.message}"] + e.backtrace).join("\n    ")
      end
    else
      def self.print_error(e)
        "#{e.class}: #{e.message}"
      end
    end

    def self.check_regex_arg(arg)
      if arg.blank?
        raise BotOperationError, 'A regular expression needs to be supplied. See https://github.com/google/re2/wiki/Syntax for syntax help.'
      end
      regex = RE2::Regexp.new(arg, one_line: true, log_errors: false)
      if regex.error
        raise BotOperationError, "Invalid regular expression: #{regex.error}. See https://github.com/google/re2/wiki/Syntax for syntax help."
      end
      return regex
    end

  end

end
