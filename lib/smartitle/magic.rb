# frozen_string_literal: true

module Smartitle

  module Magic


    ##
    # Default chances to modify title when a user is initially appeared

    USER_CHANCES_DEFAULT = 3.2


    ##
    # Maximum chances to modify title a user can have

    USER_CHANCES_MAX = 8


    ##
    # How many chances to modify title are given when a mssage is sent

    USER_CHANCES_CONVERSION_RATE = 0.02


    ##
    # Maximum number of characters in titles

    TITLE_MAX_LENGTH = 128


  end

end
