# frozen_string_literal: true

require 'sequel'
require 'active_support/core_ext/hash/compact'

module Smartitle

  class DB

    def initialize(options)
      @db_url = options[:database] || raise(ArgumentError, 'No database URL has been given')
      @logger = options[:logger] || raise(ArgumentError, 'No logger has been given')

      @groups_write_mutex = Thread::Mutex.new
      @members_write_mutex = Thread::Mutex.new
      #@changes_write_mutex = Thread::Mutex.new # Don't need a mutex yet

      @orm = Sequel.connect(@db_url)

      @orm.create_table?(:groups) do
        primary_key :group_id
        String :prefix
        TrueClass :limit_changes, null: false, default: false
        Float :chance_default
        Float :chance_max
        Float :chance_conversion_rate
      end

      @orm.create_table?(:members) do
        primary_key :id
        Integer :user_id, null: false
        Integer :group_id, null: false
        Float :chances, null: false
      end

      @orm.create_table?(:changes) do
        primary_key :id
        Integer :group_id, null: false
        Integer :time, null: false
        String :operation, null: false
        String :by, null: false
        String :title, null: false
      end
    end


    def get_group_prefix(group_id)
      @orm.from(:groups).where(group_id: group_id).get(:prefix) || ''
    end


    def set_group_prefix(group_id, content)

      @groups_write_mutex.lock

      begin
        if @orm.from(:groups).where(group_id: group_id).first
          @orm.from(:groups).where(group_id: group_id).update(prefix: content)
        else
          @orm.from(:groups).insert(group_id: group_id, prefix: content)
        end
      ensure
        @groups_write_mutex.unlock
      end

      @logger.info('DB') { "#{group_id} changed prefix to: #{content}" }
      content

    end


    def get_change_limit(group_id)
      result = @orm.from(:groups).where(group_id: group_id).select(:limit_changes, :chance_default, :chance_max, :chance_conversion_rate).first || {limit_changes: false}
      result[:chance_default]         ||= Magic::USER_CHANCES_DEFAULT
      result[:chance_max]             ||= Magic::USER_CHANCES_MAX
      result[:chance_conversion_rate] ||= Magic::USER_CHANCES_CONVERSION_RATE
      result
    end


    def set_change_limit(group_id, limit_changes: nil, chance_default: nil, chance_max: nil, chance_conversion_rate: nil)

      options = {
        limit_changes: limit_changes,
        chance_default: chance_default,
        chance_max: chance_max,
        chance_conversion_rate: chance_conversion_rate,
      }.compact

      @groups_write_mutex.lock

      begin
        if @orm.from(:groups).where(group_id: group_id).first
          @orm.from(:groups).where(group_id: group_id).update(options)
        else
          @orm.from(:groups).insert(options.merge(group_id: group_id))
        end
      ensure
        @groups_write_mutex.unlock
      end

      @logger.info('DB') { "#{group_id} changed #{options.map{|k, v| sprintf "%s: %s", k, v }.join(', ')}" }
      options

    end


    def get_member_chances(group_id, user_id)
      @orm.from(:members).where(group_id: group_id, user_id: user_id).get(:chances) || get_change_limit(group_id)[:chance_default]
    end


    def change_member_chances(group_id, user_id, offset)

      @members_write_mutex.lock

      begin
        limit_status = get_change_limit(group_id)
        old_chances = @orm.from(:members).where(group_id: group_id, user_id: user_id).get(:chances)
        new_chances = (old_chances || limit_status[:chance_default]) + offset

        if new_chances.negative?
          raise BotOperationError::NotEnoughChances, "User #{user_id} in group #{group_id} does not have enough chances. (#{old_chances})"
        elsif new_chances > limit_status[:chance_max]
          new_chances = limit_status[:chance_max]
        end

        if old_chances
          @orm.from(:members).where(group_id: group_id, user_id: user_id).update(chances: new_chances)
        else
          @orm.from(:members).insert(group_id: group_id, user_id: user_id, chances: new_chances)
        end
      ensure
        @members_write_mutex.unlock
      end

      @logger.debug('DB') { "#{user_id} in #{group_id} chances: #{old_chances} #{offset.negative? ? '-' : '+'} #{offset.abs} = #{new_chances}" }
      new_chances

    end


    def get_changes(group_id, start)
      @orm.from(:changes).where(group_id: group_id).order(Sequel.desc(:time)).limit(10, start).select(:time, :operation, :by, :title).all
    end


    def add_changes(group_id, operation: nil, by: nil, title: nil)
      @orm.from(:changes).insert(
        group_id: group_id,
        time: Time.now.to_i,
        operation: operation,
        by: by,
        title: title
      )
    end

  end

end
