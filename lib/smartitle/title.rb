# frozen_string_literal: true

module Smartitle

  class Title

    def self.from_group(group_id, bot)
      prefix = bot.db.get_group_prefix(group_id)
      text = bot.get_chat_title(group_id)
      Title.new(text, prefix: prefix)
    end


    attr_reader :source
    attr_accessor :prefix, :seperator, :segments


    def initialize(text, prefix: '', seperator: '|')
      @prefix = prefix || ''
      @seperator = seperator

      @source = text
      @segments = text.split(@seperator)
      normalize_segments!

      if @segments[0] == @prefix
        @segments.shift
      end
    end


    def normalize_segments!
      @segments = @segments.compact.map(&:strip).reject(&:empty?)
    end


    def to_s
      (!@prefix.empty? ? [@prefix] + @segments : @segments).join(' ' + @seperator + ' ')
    end


    def changed?
      to_s != @source
    end

  end

end
