# frozen_string_literal: true

module Smartitle

  VERSION = [1, 0, 0]

  def VERSION.to_s
    self.join('.')
  end

  def VERSION.inspect
    self.to_s
  end

  [:major, :minor, :patch].each_with_index do |name, index|
    VERSION.define_singleton_method name do
      self[index]
    end
  end

  VERSION.freeze

end
