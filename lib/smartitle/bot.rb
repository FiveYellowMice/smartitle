# frozen_string_literal: true

require 'logger'
require 'concurrent'
require 'active_support/core_ext/object/blank'
require 'telegram/bot'

module Smartitle

  class Bot

    attr_reader :config, :logger, :db, :available_commands


    def initialize(config)

      @config = config

      STDOUT.sync = true
      @logger = Logger.new(STDOUT)
      @logger.level = @config.debug ? Logger::DEBUG : Logger::INFO

      @logger.info('Bot') { "Starting Smartitle v#{Smartitle::VERSION}." }

      @db = DB.new(database: @config.database, logger: @logger)

      @group_mutexes = Concurrent::MVar.new({})
      @group_mutexes.define_singleton_method :[] do |key|
        borrow do |map|
          map[key] ||= Thread::Mutex.new
        end
      end

      @available_commands = {}
      CommandHandler.register_commands_to(self)

      client_th = Thread.new do
        Telegram::Bot::Client.run(@config.telegram_api_token) do |client|
          @api = client.api

          begin
            client.listen do |message|
              Concurrent::Future.execute do
                begin
                  receive(message)
                rescue => e
                  @logger.error('Bot') { Utils.print_error(e) }
                end
              end
            end
          rescue Telegram::Bot::Exceptions::ResponseError => e
            @logger.error('TelegramApi') { Utils.print_error(e) }
            retry
          rescue Faraday::ConnectionFailed => e
            @logger.error('TelegramApi') { Utils.print_error(e) }
            sleep 1
            retry
          end
        end
      end

      client_th.abort_on_exception = true

      @config.post_start && instance_eval(&@config.post_start)

      sleep

    end


    def receive(message)

      @logger.debug('Bot') {
        "Received message: [#{message.from.username || message.from.first_name + message.from.last_name.to_s}] " +
        "#{(message.text || message.caption || '(no text)').gsub("\n", " ")}"
      }

      unless ['group', 'supergroup'].include? message.chat.type
        reply_to(message, text: "Smartitle only works in groups. Please add it to a group and give \"Change group info\" permission.")
        return
      end

      begin

        if message.text && message.text =~ %r[^(?:/\w+@#{Regexp.escape(@config.telegram_username)}|/\w+(?!@))(?: |$)]

          name = /^\/(\w+)/.match(message.text)[1]
          arg = /^\/\w+(?:@\w+)? ?(.*)$/.match(message.text)[1]

          if handler = @available_commands[name]
            begin
              if handler.require_lock?
                @group_mutexes[message.chat.id].lock
              end
              handler.call(arg, message)

            ensure
              if handler.require_lock?
                @group_mutexes[message.chat.id].unlock
              end
            end
          end

        elsif message.reply_to_message && message.reply_to_message.from.username == @config.telegram_username && message.reply_to_message.text && message.reply_to_message.text.start_with?("Replace: ")
          arg = /^Replace\: (.+)$/.match(message.reply_to_message.text)[1]
          if message.text.blank? || message.text =~ /\||\n/
            reply_to(message,
              disable_web_page_preview: true,
              reply_markup: Telegram::Bot::Types::ForceReply.new(force_reply: true, selective: true),
              text: "Replace: #{arg}\nReplacement text needs to be a one-line text contains no disallowed characters. Please supply the replacement text again:")
          else

            @group_mutexes[message.chat.id].lock
            begin
              spend_chance(message.chat.id, message.from.id)
              regex = Utils.check_regex_arg(arg)
              title = Title.from_group(message.chat.id, self)
              title.segments = title.segments.map do |segment|
                segment.re2_gsub(regex, message.text)
              end
              title.normalize_segments!
              if !title.changed?
                raise BotOperationError, 'Title unchanged because no matches were found.'
              end
              set_chat_title(message.chat.id, title.to_s)
              @db.add_changes(message.chat.id, operation: 'replace', by: stringify_user(message.from), title: title.to_s)

            ensure
              @group_mutexes[message.chat.id].unlock
            end
          end

        elsif message.new_chat_title

          if message.from.username != @config.telegram_username
            @logger.info('Group') { "#{message.chat.id} title changed by other member: #{message.new_chat_title}" }

            begin
              @group_mutexes[message.chat.id].lock

              title = Title.from_group(message.chat.id, self)
              if title.changed?
                set_chat_title(message.chat.id, title.to_s)
                @db.add_changes(message.chat.id, operation: 'normalize', by: stringify_user(message.from), title: title.to_s)
              end

            ensure
              @group_mutexes[message.chat.id].unlock
            end
          end

        else
          limit_status = @db.get_change_limit(message.chat.id)
          @db.change_member_chances(message.chat.id, message.from.id, limit_status[:chance_conversion_rate])

        end

      rescue BotOperationError => e
        reply_to(message, disable_web_page_preview: true, text:
                 (e.message || 'Something happened.') +
                "\nSend /help@#{@config.telegram_username} for more help."
                )

      rescue => e
        reply_to(message, disable_web_page_preview: true, text:
                 "An unexpected error occured during the operation.\n" +
                "#{e.class}: #{e.message}" +
                "\nPlease try again. If the problem persists, please file an issue on https://fym.one/smartitle-issues ."
                )
        @logger.error('Bot') { Utils.print_error(e) }

      end

    end


    def register_command(names, description, options = {}, &block)
      unless names.respond_to? :each
        names = [names]
      end
      handler = CommandHandler.new(self, names[0], description, options, &block)
      names.each do |name|
        @available_commands[name] = handler
      end
    end


    def reply_to(message, options)
      new_options = options.clone
      new_options[:chat_id] = message.chat.id
      new_options[:reply_to_message_id] = message.message_id
      @api.send_message(new_options)
    end


    def get_chat_title(group_id)
      @api.get_chat(chat_id: group_id)['result']['title']
    end


    def set_chat_title(group_id, title)

      if title.blank?
        raise BotOperationError, 'Title can not be empty.'
      elsif title.length > Smartitle::Magic::TITLE_MAX_LENGTH
        raise BotOperationError::TitleTooLong, "Title is too long (#{title.length})."
      end

      begin
        @api.call('setChatTitle', chat_id: group_id, title: title)
        @logger.info('Group') { "#{group_id} changed title to: #{title}" }

      rescue Telegram::Bot::Exceptions::ResponseError => e
        if e.message.include? 'not enough rights to change chat title'
          raise BotOperationError, "Smartitle does not have sufficient permission to change title, please go to group settings and enable \"Change group info\" permission on the bot."
        else
          raise e
        end
      end

    end


    def pin_message(message)
      begin
        @api.call('pinChatMessage', chat_id: message.chat.id, message_id: message.message_id, disable_notification: true)
        @logger.info('Group') { "#{message.chat.id} pinned message: #{message.text || '(no text)'}" }

      rescue Telegram::Bot::Exceptions::ResponseError => e
        if e.message.include? 'not enough rights to pin a message'
          raise BotOperationError, "The administrators of this group have not allowed Smartitle to pin messages. If you are an administrator and want to change that, please go to group settings and enable \"Pin messages\" permission on the bot."
        else
          raise e
        end
      end
    end


    def stringify_user(user)
      user.username || [user.first_name, user.last_name].compact.join(' ')
    end


    def spend_chance(group_id, user_id)
      if @db.get_change_limit(group_id)[:limit_changes]
        begin
          @db.change_member_chances(group_id, user_id, -1)
        rescue BotOperationError::NotEnoughChances
          raise BotOperationError, 'You have run out of chances to change title. Engage more in chatting to get more chances!'
        end
      end
    end


    ##
    # Check if a user is an admin and has "Change group info" permission

    def check_admin(group_id, user_id)
      admin_ids = @api.get_chat_administrators(chat_id: group_id)['result'].select{|c| c['status'] == 'creator' || c['can_change_info'] }.map{|c| c['user']['id'] }
      admin_ids.include? user_id
    end


    def check_admin!(group_id, user_id)
      unless check_admin(group_id, user_id)
        raise BotOperationError, 'Only administrators are allowed to perform this operation.'
      end
    end


  end

end
