# frozen_string_literal: true

require 'active_support/core_ext/object/blank'

module Smartitle

  class CommandHandler


    attr_reader :bot, :name, :description, :require_lock, :block

    alias_method :require_lock?, :require_lock


    ##
    # Make a new command handler
    # @param bot [Smartitle::Bot] bot the handler belongs to
    # @param name [String] name(s) of the command
    # @param description [String] description of the command
    # @param require_lock [Boolean] whether the command requires a lock
    # @param block [Proc] block to run when command is invoked
    # @yieldparam arg [String] arguments to the command
    # @yieldparam message [Telegram::Bot::Types::Message] message

    def initialize(bot, name, description = nil, require_lock: false, &block)
      @bot = bot
      @logger = @bot.logger
      @name = name || raise(ArgumentError, "Command must have a name.")
      @description = description
      @require_lock = require_lock
      @block = block
    end


    def call(arg, message)
      instance_exec(arg, message, &@block)
    end


    ##
    # Register commands

    def self.register_commands_to(bot)

      bot.register_command('help', "Show help.") do |arg, message|
        help_message_head = <<~HTML
          Smartitle v#{Smartitle::VERSION}
          Changing title made smart.
          <a href="https://gitlab.com/FiveYellowMice/smartitle">Project Repository</a>
          This program is free software, see <a href="http://www.gnu.org/licenses/">the terms of the GNU General Public License</a> for more information.

          Usage:
        HTML
        @bot.reply_to(message,
          disable_web_page_preview: true,
          parse_mode: 'HTML',
          text: help_message_head + "\n" + @bot.available_commands.reduce([]) do |memo, (name, handler)|
            if existing = memo.find {|a| a[:description] == handler.description }
              existing[:names] << name
            else
              memo << {names: [name], description: handler.description}
            end
            memo
          end.map do |obj|
            obj[:names].map{|n| "<code>/#{n}</code>" }.join("\n") + "\n#{obj[:description]}"
          end.join("\n\n")
        )
      end

      bot.register_command('read', "Read full title.") do |arg, message|
        title = Title.from_group(message.chat.id, @bot)
        if title.prefix
          @bot.reply_to(message, text: ([title.prefix] + title.segments).join("\n    "))
        else
          @bot.reply_to(message, text: title.segments.join("\n"))
        end
      end

      bot.register_command(['add', 'prepend', 'unshift'], "Add text before current title.", require_lock: true) do |arg, message|
        if arg.blank?
          if message.reply_to_message && !message.reply_to_message.text.blank?
            arg = message.reply_to_message.text
          else
            raise BotOperationError, 'No text is given.'
          end
        end
        if arg =~ /\||\n/
          raise BotOperationError, 'Text contains disallowed characters.'
        end
        @bot.spend_chance(message.chat.id, message.from.id)
        title = Title.from_group(message.chat.id, @bot)
        title.segments.unshift(arg)
        begin
          @bot.set_chat_title(message.chat.id, title.to_s)
        rescue BotOperationError::TitleTooLong => e
          title.segments.pop
          if title.segments.length > 1
            retry
          else
            raise e
          end
        end
        @bot.db.add_changes(message.chat.id, operation: 'add', by: @bot.stringify_user(message.from), title: title.to_s)
      end

      bot.register_command('pop', "Remove last segment in title.", require_lock: true) do |arg, message|
        @bot.spend_chance(message.chat.id, message.from.id)
        title = Title.from_group(message.chat.id, @bot)
        title.segments.pop
        @bot.set_chat_title(message.chat.id, title.to_s)
        @bot.db.add_changes(message.chat.id, operation: 'pop', by: @bot.stringify_user(message.from), title: title.to_s)
      end

      bot.register_command(['remove', 'shift'], "Remove newest segment in title.", require_lock: true) do |arg, message|
        @bot.spend_chance(message.chat.id, message.from.id)
        title = Title.from_group(message.chat.id, @bot)
        title.segments.shift
        @bot.set_chat_title(message.chat.id, title.to_s)
        @bot.db.add_changes(message.chat.id, operation: 'remove', by: @bot.stringify_user(message.from), title: title.to_s)
      end

      bot.register_command(['push', 'append'], "Add text after current title. (unrecommended)", require_lock: true) do |arg, message|
        if arg.blank?
          if message.reply_to_message && !message.reply_to_message.text.blank?
            arg = message.reply_to_message.text
          else
            raise BotOperationError, 'No text is given.'
          end
        elsif arg =~ /\||\n/
          raise BotOperationError, 'Text contains disallowed characters.'
        end
        @bot.spend_chance(message.chat.id, message.from.id)
        title = Title.from_group(message.chat.id, @bot)
        title.segments.push(arg)
        @bot.reply_to(message, text: "Adding text after title is not recommended. New text should be added in the front to get more impressions. Use /undo@#{@bot.config.telegram_username} to undo this, use /add@#{@bot.config.telegram_username} command to add text in front.")
        @bot.set_chat_title(message.chat.id, title.to_s)
        @bot.db.add_changes(message.chat.id, operation: 'push', by: @bot.stringify_user(message.from), title: title.to_s)
      end

      bot.register_command(['join', 'implode', 'fusion', 'concat'], "Join all segments besides prefix into one.", require_lock: true) do |arg, message|
        @bot.spend_chance(message.chat.id, message.from.id)
        title = Title.from_group(message.chat.id, @bot)
        title.segments = [title.segments.join(arg)]
        title.normalize_segments!
        if !title.changed?
          raise BotOperationError, 'Title unchanged because there was nothing to join.'
        end
        @bot.set_chat_title(message.chat.id, title.to_s)
        @bot.db.add_changes(message.chat.id, operation: 'join', by: @bot.stringify_user(message.from), title: title.to_s)
      end

      bot.register_command(['split', 'explode'], "Split title into segments with a RegEx.", require_lock: true) do |arg, message|
        regex = Utils.check_regex_arg(arg)
        @bot.spend_chance(message.chat.id, message.from.id)
        title = Title.from_group(message.chat.id, @bot)
        title.segments = title.segments.map{|s| s.re2_gsub(regex, title.seperator).split(title.seperator) }.flatten
        title.normalize_segments!
        if !title.changed?
          raise BotOperationError, 'Title unchanged because no matches were found.'
        end
        @bot.set_chat_title(message.chat.id, title.to_s)
        @bot.db.add_changes(message.chat.id, operation: 'split', by: @bot.stringify_user(message.from), title: title.to_s)
      end

      bot.register_command('replace', "Replace text in title that matches a RegEx. You will be asked for replacement text afterwards.", require_lock: false) do |arg, message|
        Utils.check_regex_arg(arg)
        @bot.reply_to(message,
          disable_web_page_preview: true,
          reply_markup: Telegram::Bot::Types::ForceReply.new(force_reply: true, selective: true),
          text: "Replace: #{arg}\nPlease supply the replacement text:")
      end

      bot.register_command('clear', "Clear title, preserves prefix or the first segment.", require_lock: true) do |arg, message|
        @bot.spend_chance(message.chat.id, message.from.id)
        title = Title.from_group(message.chat.id, @bot)
        if !title.prefix.blank?
          title.segments = []
        else
          title.segments = [title.segments[0]]
        end
        @bot.set_chat_title(message.chat.id, title.to_s)
        @bot.db.add_changes(message.chat.id, operation: 'clear', by: @bot.stringify_user(message.from), title: title.to_s)
      end

      bot.register_command('undo', "Undo last change.", require_lock: true) do |arg, message|
        changes = @bot.db.get_changes(message.chat.id, 0)
        if changes.length < 2
          raise BotOperationError, "Unable to undo without sufficient history."
        elsif changes[0][:operation] == 'undo'
          raise BotOperationError, "Only undoing 1 step is currently supported."
        end
        @bot.set_chat_title(message.chat.id, changes[1][:title])
        @bot.db.add_changes(message.chat.id, operation: 'undo', by: @bot.stringify_user(message.from), title: changes[1][:title])
      end

      bot.register_command('pin', "Pin a message as a normal member.", require_lock: false) do |arg, message|
        if !message.reply_to_message
          raise BotOperationError, "Reply to a message when using this command to pin it."
        end
        @bot.pin_message(message.reply_to_message)
      end

      bot.register_command('set_prefix', "Set prefix that is always at the start of title.", require_lock: true) do |arg, message|
        @bot.check_admin!(message.chat.id, message.from.id)
        if arg =~ /\||\n/
          raise BotOperationError, 'Text contains disallowed characters.'
        elsif arg.length > Smartitle::Magic::TITLE_MAX_LENGTH
          raise BotOperationError, 'Prefix can not excceed title length limit.'
        end
        title = Title.from_group(message.chat.id, @bot)
        title.prefix = arg
        @bot.db.set_group_prefix(message.chat.id, arg)
        @bot.set_chat_title(message.chat.id, title.to_s)
        @bot.db.add_changes(message.chat.id, operation: 'set_prefix', by: @bot.stringify_user(message.from), title: title.to_s)
        @bot.reply_to(message, text: "Prefix has been #{arg.empty? ? 'cleared' : 'set'}.")
      end

      bot.register_command('limit_status', "Show limit status of this group and the chances you have.") do |arg, message|
        limit_status = @bot.db.get_change_limit(message.chat.id)
        member_chances = @bot.db.get_member_chances(message.chat.id, message.from.id)
        text = <<~HTML
          <b>This group</b>
          Change limit enabled: #{limit_status[:limit_changes]}
          Default chance: #{limit_status[:chance_default]}
          Maximum chance: #{limit_status[:chance_max]}
          Chance conversion rate: #{limit_status[:chance_conversion_rate]}

          <b>You</b>
          Chances remaining: #{'%0.3f' % member_chances}
        HTML
        @bot.reply_to(message, text: text, parse_mode: 'HTML')
      end

      bot.register_command('enable_change_limit', "Enable change limit.") do |arg, message|
        @bot.check_admin!(message.chat.id, message.from.id)
        @bot.db.set_change_limit(message.chat.id, limit_changes: true)
        @bot.reply_to(message, text: 'Users will now have limited chances to change title.')
      end

      bot.register_command('disable_change_limit', "Disable change limit.") do |arg, message|
        @bot.check_admin!(message.chat.id, message.from.id)
        @bot.db.set_change_limit(message.chat.id, limit_changes: false)
        @bot.reply_to(message, text: 'Users will no longer have limited chances to change title.')
      end

      bot.register_command('set_default_chance', "Set the default chance a user initially has.") do |arg, message|
        @bot.check_admin!(message.chat.id, message.from.id)
        if arg.blank?
          raise BotOperationError, 'No value given.'
        end
        @bot.db.set_change_limit(message.chat.id, chance_default: arg.to_f)
        @bot.reply_to(message, text: 'Default chance has been set.')
      end

      bot.register_command('set_max_chance', "Set the maximum chance a user can have.") do |arg, message|
        @bot.check_admin!(message.chat.id, message.from.id)
        if arg.blank?
          raise BotOperationError, 'No value given.'
        end
        @bot.db.set_change_limit(message.chat.id, chance_max: arg.to_f)
        @bot.reply_to(message, text: 'Max chance has been set.')
      end

      bot.register_command('set_chance_conversion_rate', "Set how many chances are given when a mssage is sent.") do |arg, message|
        @bot.check_admin!(message.chat.id, message.from.id)
        if arg.blank?
          raise BotOperationError, 'No value given.'
        end
        @bot.db.set_change_limit(message.chat.id, chance_conversion_rate: arg.to_f)
        @bot.reply_to(message, text: 'Chance conversion rate has been set.')
      end

      bot.register_command('history', "Show title change history.") do |arg, message|
        offset = [arg.to_i, 0].max
        changes = @bot.db.get_changes(message.chat.id, offset)
        if changes.empty?
          text = 'Nothing to show.'
        else
          text = changes.map.with_index(offset + 1) do |item, index|
            "#{sprintf('%02d', index)} | #{Time.at(item[:time])} #{item[:operation].rjust(5)} by #{item[:by]}: #{item[:title]}"
          end.join("\n")
        end
        @bot.reply_to(message, text: text)
      end

    end


  end

end
