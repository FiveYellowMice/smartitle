# frozen_string_literal: true

module Smartitle

  autoload :Bot, 'smartitle/bot'
  autoload :CommandHandler, 'smartitle/command_handler'
  autoload :Title, 'smartitle/title'
  autoload :Config, 'smartitle/config'
  autoload :Utils, 'smartitle/utils'
  autoload :BotOperationError, 'smartitle/bot_operation_error'
  autoload :DB, 'smartitle/db'
  autoload :Magic, 'smartitle/magic'
  autoload :VERSION, 'smartitle/version'

  def self.start
    config = Config.new(ARGV)
    Bot.new(config)
  end

end
