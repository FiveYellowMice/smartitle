# Username of the bot, without preceding '@'
@telegram_username = ENV['SMARTITLE_TELEGRAM_USERNAME']

# Telegram API token, acquired from BotFather
@telegram_api_token = ENV['SMARTITLE_TELEGRAM_API_TOKEN']

# Database URL, see http://sequel.jeremyevans.net/rdoc/files/doc/opening_databases_rdoc.html
@database = 'sqlite://' + File.expand_path('var/production.db', Dir.pwd)

# Run code after the bot starts up
@post_start = proc do
  #puts 'OK'
end

# Additional gems needed for database
@database_gems = proc do
  gem 'sqlite3', '~> 1.6'
end
