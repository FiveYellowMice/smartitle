# Smartitle

_Changing title made smart._

Smartitle is a Telegram bot to increase group member engagement. It allows normal group members to make changes to the group title (group name) in a controlled and fun way.

![Example](example.png)

The group title will be formatted as an array of small segments seperated by ` | `, they can be added and removed by anyone in the group (that can send messages). The content of them are completely up to everyone, so there can be plenty of uses, labeling current mood, hanging memes, recording 'important' moments... The only limit is your creativity! Or... the 255 character length limit imposed by Telegram.

Admins can also set a prefix that always stays as the first segment, so your group will never lose identity because of it changing name all the time.


## Usage

Add [this bot](https://t.me/smartitle_bot) (or host your own) to your group, then give it prvilege to "Change group info".

Read the [user manual](doc/user_manual.md).


## Copyright

Copyright (C) 2017-2019  FiveYellowMice

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
